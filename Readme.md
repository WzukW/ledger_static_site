# Ledger static site generator

[![license](https://img.shields.io/github/license/mashape/apistatus.svg?style=for-the-badge)](https://gitlab.com/WzukW/ledger_static_site/blob/master/LICENSE.txt)

**This is primary a personnal project, however, I’m happy to share it with anyone. Get in touch!**

## Goal

If you use (and you should ;-)) [ledger](https://www.ledger-cli.org/) to keep your finance ordered, you know how awesome this tool is. But it lacks some graphing capability, especially considering pie graph.

We propose a simple Python script to generate a set of html file, one per account, with desirable graph.

## Installation

### Dependancies

It is design to be easy to use and thus dependancies are minimal:

 - [jinja2](http://jinja.pocoo.org/), for templates,
 - [pygal](http://pygal.org/en/stable/), for graphs.

### That’s all

## Usage

Just clone the repository and run

``` bash
$ python3 produceGraph.py my/ledgerFile.dat
```

you will find an html file per account, in the `output` folder.

You may put it on a webserver (allow to use internal links) or browse it locally (open one file after the other).
