#!/bin/env python3
#encoding: utf-8
#
# Copyright 2018 Clément Joly
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Produces graph from ledger (ledger-cli.org) xml output.

from functools import reduce
import datetime
import sys
import xml.etree.ElementTree as ET
from jinja2 import Environment, FileSystemLoader, select_autoescape
import os
import pygal
import subprocess

def amountPerSubAccount(parentAccount):
    """ Get a list of name and total absolute amount for 2 first descendants of the parent account """
    def accountBalance(account):
        """ Value in an account """
        amount = account.find('account-total/amount/quantity')
        if amount is None: return 0
        return abs(float(amount.text))

    def subTotal(account):
        """ Gives a list of account xml tree, name and amounts"""
        if account is None: return []

        sub = []
        for subAccount in account.findall('account'):
            name = subAccount.find('name')
            fullname = subAccount.find('fullname')
            sub.append({
                "xml": subAccount,
                "name": name.text if name is not None else None,
                "fullname": fullname.text if fullname is not None else None,
                "bal": accountBalance(subAccount)
            })

        # An account may contain more than the sum of sub accounts
        totalBalance = reduce(lambda sum, a: a["bal"] + sum, sub, 0)
        diff = accountBalance(account) - totalBalance
        if diff > 0: sub.append({
                "xml": None,
                "name": None,
                "fullname": None,
                "bal": diff})
        return sub

    def addChildrenAndPrepare(accountTuple):
        """ Add children account and adapt informations to use in the graph """
        xml = accountTuple["xml"]
        if xml == None: return(None, accountTuple["bal"])
        l2Accounts = subTotal(xml)
        l2Values = map(
            lambda account: {
                'label': account["name"] if account["name"] is not None else "",
                'value': account["bal"]
            },
            l2Accounts)
        return (accountTuple["name"], list(l2Values))

    data = subTotal(parentAccount)
    data = map(addChildrenAndPrepare, data)

    return list(data)

def pieChart(name_value, title=None):
    """ Create pie chart from tuple list """
    chart = pygal.Pie(dynamic_print_values=True, half_pie=True)
    chart.title = title
    for a in name_value: chart.add(a[0], a[1])
    return chart

def genXMLReport(ledgerFile, reportQuery):
    """ Returns ledger xml report, parsed """
    XMLReport = subprocess.run(
        ([ 'ledger', '-f', ledgerFile, 'xml' ] + reportQuery),
        stdout=subprocess.PIPE).stdout.decode('utf-8')
    return ET.fromstring(XMLReport)

def genAccountReport(ledgerFile, reportQuery):
    """ Returns a XML tree from the ledger xml report corresponding to command,
        along with list of account nodes """
    XMLReport = genXMLReport(ledgerFile, reportQuery)

    accountNames = list(map(
        lambda a: a.text,
        XMLReport.findall('.//account/fullname')))

    return XMLReport, accountNames

def genBalancesWithDate(ledgerFile, accountName):
    """ Gives a list of dates associated with balances for a given account, prepared for graphing """

    def getAmount(root):
        return float(root.find('postings/posting/total/amount/quantity').text)

    def getDate(root):
        # XXX Manual parsing
        dateString = root.find('./date').text
        # Format YYYY/MM/DD
        dateSplit = list(map(lambda s: int(s) , dateString.split('/')))
        date = datetime.date(dateSplit[0], dateSplit[1], dateSplit[2])
        return date

    def getCommodity(root):
        """ Return symbol of the commodity of a transaction """
        return root.find(
            './postings/posting/total/amount/commodity/symbol'
        ).text

    def forPygal(commodities, transaction):
        """ Prepare for graphing library , one set of data per commodity"""
        commodity = getCommodity(transaction)
        date = getDate(transaction)
        amount = getAmount(transaction)
        if commodity not in commodities:
            commodities[commodity] = [( date, amount )]
        else:
            commodities[commodity].append((date, amount))
        return commodities

    if accountName is None:
        return {}
    XMLReport = genXMLReport(ledgerFile, [ 'reg', accountName])
    transactions = XMLReport.findall('./transactions/transaction')
    dateAmountPerCommodity = reduce(forPygal, transactions, {})

    for k in dateAmountPerCommodity:
        dateAmountPerCommodity[k].sort()

    return dateAmountPerCommodity

def getTreeAndDepthLevel(accountName):
    """ Depth level: 'Assets' -> 1, 'Assets:Gold' -> 2
        Tree: 'Assets' -> 'Assets', 'Assets:Gold' -> [ 'Assets', 'Gold' ] """
    if accountName is not None:
        tree = accountName.split(':')
    else: tree = []
    return (tree, len(tree))


def genSubAccounts(allAccounts, treeCache, parentAccountName):
    """ Generate a list of name of sub-accounts of parent account, tree cache
        is expected to be a dictionnary on account name containing a tuple,
        in which second value is the depth level of the given account """

    pAN = "" if parentAccountName is None else parentAccountName
    lPAN = len(parentAccountName) if parentAccountName is not None else 0
    def concatenedTo(b):
        """ Checks if b is the concatenation of parentAccountName and
            something """
        return (len(b)>lPAN) and (b[:lPAN]==pAN)
    def correctDepthAndPrefix(a):
        correctDepth = treeCache[a][1] == currentDepth + 1
        return correctDepth and concatenedTo(a)

    currentDepth = treeCache[parentAccountName][1]
    filteredReport = list(filter(
        correctDepthAndPrefix,
        allAccounts))

    return filteredReport

def renderSvgChart(chart):
    """ Gives svg tree to be inserted within webpage """
    return chart.render(
        unicode=True,
        legend_at_bottom=True,
        disable_xml_declaration=True)

def genDateLineGraph(data, formatWithSerieName=False):
    """ Generate a XY graph like
        http://pygal.org/en/stable/documentation/types/xy.html#date.
        If formatWithSerieName is True, name of serie is used as unit for
        displayed values """
    dateTL = pygal.DateLine(fill=True)
    for serie in data.keys():
        if formatWithSerieName: fmtr = lambda x: "%s %s" % (x[1], serie)
        else: fmtr = None
        dateTL.add(serie, data[serie], formatter=fmtr)
    return dateTL


def genPieChart(XMLTree, title, reportName):
    """ Generate a pie chart from XML tree of a parent account """
    parentAccount = XMLTree.find('.//account[fullname="%s"]' % title)
    accounts = amountPerSubAccount(parentAccount)
    return pieChart(accounts, "%s - %s" % (title, reportName))

def renderAccountTemplate(env, account, svgTreeGraphs, outDir):
    """ Render jinja2 template for account """
    template = env.get_template('account.jinja2')
    content = template.render(
        {'account': account,
         'svgTreeGraphs': svgTreeGraphs,
        })
    accountFullname = account["fullname"]
    accountFileName = accountFullname + ".html" if accountFullname is not None else "index.html"
    accountFileName = outDir + "/" + accountFileName
    with open(accountFileName, 'w') as f:
        f.write(content)

def mkOutputDir(outDir):
    """ Create output directory, if not existing, to store generated html """
    try: os.mkdir(outDir)
    except FileExistsError: return
    return

def main():
    if len(sys.argv) != 2:
        print("One argument expected, your ledger log")
        return
    ledgerFile = sys.argv[1]

    # Output dirrectory
    outDir = "output"
    mkOutputDir(outDir)

    env = Environment(
        loader=FileSystemLoader('./template')
        # TODO Use this
        # autoescape=select_autoescape(['html', 'xml'])
    )

    reportNames = [ 'all time', 'this year', 'last month', 'this month' ]
    reportQueries = {
        'all time': [],
        'this year': [ '-b', 'january' ],
        'last month': [ '-b', 'last month', '-e', 'this month'],
        'this month': [ '-b', 'this month' ]
    }
    reports = {}
    for reportName in reportNames:
        xml, accounts = genAccountReport(ledgerFile, reportQueries[reportName])
        currentReport = {
            "name": reportName,
            "xml": xml,
            "accounts": accounts
        }
        reports[reportName] = currentReport
    accounts = reports['all time']['accounts']

    treeCache = {}
    for account in accounts:
        treeCache[account] = getTreeAndDepthLevel(account)

    for account in accounts:
        svgTreeGraphs = {}
        for reportName in reportNames:

            if treeCache[account][0] != []:
                concatTreeFirst = [(treeCache[account][0][0], treeCache[account][0][0])]
            else: concatTreeFirst = []
            concatTree = concatTreeFirst + [
                (
                    reduce(lambda a, b: a + ':' + b, treeCache[account][0][:i+1]),
                    treeCache[account][0][i]
                )
                for i in range(1, len(treeCache[account][0]))
            ]

            pieGraph = genPieChart(reports[reportName]["xml"], account, reportName)
            svgTreeGraphs[reportName] = renderSvgChart(pieGraph)
            accountForTemplate = {
                "fullname": account,
                "tree": treeCache[account],
                "concatTree": concatTree,
                "subAccounts": genSubAccounts(accounts, treeCache, account)
            }

        dateTL = genBalancesWithDate(ledgerFile, account)
        dateTLGraph = genDateLineGraph(dateTL, formatWithSerieName=True)
        svgTreeGraphs['dateTL'] = renderSvgChart(dateTLGraph)

        renderAccountTemplate(env, accountForTemplate, svgTreeGraphs, outDir)

if __name__ == "__main__":
    main()
